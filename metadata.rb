name             'thoroughcare-cookbook'
maintainer       'Levvel, Inc.'
maintainer_email 'ben.vandgrift@levvel.io'
license          'all_rights'
description      'recipes for thoroughcare/opsworks'
long_description 'recipes for thoroughcare/opsworks'
version          '0.1.2'

provides "thoroughcare-cookbook::create_environment"
provides "thoroughcare-cookbook::symlink_environment"
