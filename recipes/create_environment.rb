#
# Cookbook Name:: thoroughcare-cookbook
# Recipe:: create_environment
#
# create a dotenv file from template or default.

envt = node['environment']

if envt && envt.any?
  template '/srv/www/thoroughcare/shared/.env' do
    variables vars: envt
    source 'dotenv.erb'
    owner 'deploy'
    group 'www-data'
    mode  '0660'
  end
else
  file '/srv/www/thoroughcare/shared/.env' do
    owner 'deploy'
    group 'www-data'
    content "CACHE_ENABLE=false\n"
    mode '0660'
  end
  Chef::Log.warn("missing or empty node['environment'].")
end
