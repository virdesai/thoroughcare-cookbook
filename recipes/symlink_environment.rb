#
# Cookbook Name:: thoroughcare-cookbook
# Recipe:: symlink_environment
#
# create a dotenv file from template or default.

envt = node['environment']

if envt && envt.any?
  link '/srv/www/thoroughcare/current/.env' do
    to '/srv/www/thoroughcare/shared/.env'
    owner 'deploy'
    group 'www-data'
    mode  '0660'
  end
else
  Chef::Log.warn("missing or empty node['environment'], not symlinking")
end
