execute "set_cron_user" do
  Chef::Log.info("running whenever gem : sudo bundle exec whenever --update-crontab -r root")
  command "cd /srv/www/thoroughcare/current && sudo bundle exec whenever --update-crontab -r root"
  action :run
end

